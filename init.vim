" Specify a directory for plugins
call plug#begin('~/.local/share/nvim/plugged')

" NERDTree
Plug 'scrooloose/nerdtree'
" LightLine
Plug 'itchyny/lightline.vim'
" File icons
Plug 'ryanoasis/vim-devicons'
" Git branch info in LightLine
Plug 'itchyny/vim-gitbranch'
" buffers in LightLine tabs line
Plug 'mengelbrecht/lightline-bufferline'
" FuzzyFinder
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
" Git support
Plug 'tpope/vim-fugitive'
" Git status in NERDTree
Plug 'Xuyuanp/nerdtree-git-plugin'
" Comment
Plug 'tpope/vim-commentary'
" Exchange text region
Plug 'tommcdo/vim-exchange'
" Autocompletion
Plug 'ncm2/ncm2'
" Autocompletion using buffer
Plug 'ncm2/ncm2-bufword'
" Autocompletion using path
Plug 'ncm2/ncm2-path'
" Remote plugin framework
Plug 'roxma/nvim-yarp'
" ALE Linting engine
Plug 'dense-analysis/ale'
" Snipets
Plug 'Shougo/neosnippet.vim'
Plug 'ncm2/ncm2-neosnippet'
Plug 'Shougo/neosnippet-snippets'
" Autoclose brackets
Plug 'Townk/vim-autoclose'
" nord colorscheme
Plug 'arcticicestudio/nord-vim'
" TOML support
Plug 'cespare/vim-toml'
" Latex support
Plug 'LaTeX-Box-Team/LaTeX-Box'

" Autocompletion for rust
Plug 'ncm2/ncm2-racer'
" Syntax for rust
Plug 'rust-lang/rust.vim'

" Autocompletion for python
Plug 'ncm2/ncm2-jedi'
" Python Formatter
Plug 'psf/black'

" HTML support
Plug 'mattn/emmet-vim'

" Initialize plugin system
call plug#end()

" -------- NERDTree Start --------

" Toggle NERDTree
nnoremap <C-e> :NERDTreeToggle<CR>

" -------- NERDTree End ----------

" -------- FZF Start --------

source /usr/share/doc/fzf/examples/fzf.vim
function! RipgrepFzf(query)
  let command_fmt = 'rg --column --line-number --no-heading --color=always --smart-case -- %s || true'
  let initial_command = printf(command_fmt, shellescape(a:query))
  let reload_command = printf(command_fmt, '{q}')
  let options = {'options': ['--phony', '--query', a:query, '--bind', 'change:reload:'.reload_command]}
  let options = fzf#vim#with_preview(options)
  call fzf#vim#grep(initial_command, 1, options, 1)
endfunction
command! -bang -nargs=* Rg call RipgrepFzf(<q-args>)
" Toggle pattern search
nnoremap <C-F> :Rg<CR>
" Toggle file search
nnoremap <C-P> :Files<CR>

" -------- FZF End ----------

" -------- LightLine Start ----------

let g:lightline = {
    \ 'active': {
      \   'left': [[ 'mode', 'paste' ], [ 'gitbranch', 'readonly', 'filename', 'modified' ]],
      \   'right': [['lineinfo'], ['percent'], ['fileformat', 'fileencoding', 'filetype']],
    \ },
    \ 'colorscheme': 'nord',
    \ 'component_expand': {
      \ 'buffers': 'lightline#bufferline#buffers',
    \ },
    \ 'component_function': {
      \ 'filetype': 'DisplayFileTypeSymbols',
      \ 'fileformat': 'DisplayFileFormatSymbol',
      \ 'gitbranch': 'gitbranch#name',
    \ },
    \ 'component_type': {
      \ 'buffers': 'tabsel',
    \ },
    \ 'mode_map': {
      \ 'n' : 'N',
      \ 'i' : 'I',
      \ 'R' : 'R',
      \ 'v' : 'V',
      \ 'V' : 'VL',
      \ "\<C-v>": 'VB',
      \ 'c' : 'C',
      \ 's' : 'S',
      \ 'S' : 'SL',
      \ "\<C-s>": 'SB',
      \ 't': 'T',
    \ },
    \ 'tabline': {
      \ 'left': [ ['buffers'] ],
      \ 'right': [ ['close'] ],
    \ }
  \ }

let g:lightline#bufferline#enable_devicons=1

function! DisplayFileTypeSymbols()
  return winwidth(0) > 70 ? (WebDevIconsGetFileTypeSymbol()) : ''
endfunction

function! DisplayFileFormatSymbol()
  return winwidth(0) > 70 ? (WebDevIconsGetFileFormatSymbol()) : ''
endfunction

" -------- LightLine End ----------

" -------- NCM2 Start --------

" enable ncm2 for all buffers
autocmd BufEnter * call ncm2#enable_for_buffer()

" IMPORTANT: :help Ncm2PopupOpen for more information
set completeopt=noinsert,menuone,noselect
" Leave completion popup with escape
inoremap <C-c> <ESC>

" -------- NCM2 End ----------

" -------- ALE Start ----------

let g:ale_linters = {'rust': ['analyzer'], 'python': ['pyls', 'pylint']}
let g:ale_rust_analyzer_executable = '/home/airone/.local/bin/rust-analyzer'
let g:ale_fixers = {'rust': ['rustfmt'], 'python': ['black']}
let g:ale_fix_on_save = 1

" Go to definition with F12
nnoremap <silent> <F12> :ALEGoToDefinition<Return>
nnoremap <silent> <Plug>(ale_go_to_definition) :ALEGoToDefinition<Return>

"Go to next error with F8
nnoremap <silent> <F8> :ALENext<Return>
nnoremap <silent> <Plug>(ale_next) :ALENext<Return>

" -------- ALE End ----------

" The parameters are the same as `:help feedkeys()`
inoremap <silent> <expr> <CR> ncm2_neosnippet#expand_or("\<CR>", 'n')

" -------- Python Start --------
let g:black_virtualenv = "~/.local/share/nvim/plugged/black"
" -------- Python End ----------

" -------- Neosnippet Start ----------
" Plugin key-mappings.
" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)
" -------- Neosnippet End ----------

" -------- Editor customisation Start ----------

" Display line number
set number
" Insert space characters whent tab key is pressed.
set expandtab
" Number of space character inserted when tab key is used.
set tabstop=4
" Fine tunes the amount of whitespace to be inserted.
set softtabstop=4
" Number of space character used for indent.
set shiftwidth=4

noremap <Tab> :bn<CR>
noremap <S-Tab> :bp<CR>
noremap <Leader><Tab> <C-^>
noremap <Leader><S-Tab> :bw<CR>

" Whitespace trailing
nnoremap <silent> <F5> :call <SID>strip_trailling_whitespaces()<CR>
function! <SID>strip_trailling_whitespaces()
    " Preparation: save last search and cursor position.
    let _s=@/
    let l = line(".")
    let c = col(".")
    " Do the business
    %s/\s\+$//e
    " Clean up: restore previous search history and cursor position
    let @/=_s
    call cursor(l, c)
endfunction

" Allow to change buffer even if the current one is not written
set hidden

" Do not show vim mode
set noshowmode
" Always display tabline
set showtabline=2

" Shows the effects of a command incrementally, as you type.
set inccommand=nosplit

" smartcase search
set ignorecase
set smartcase

set termguicolors
colorscheme nord

" Load all plugins now.
" Plugins need to be added to runtimepath before helptags can be generated.
packloadall
" Load all of the helptags now, after plugins have been loaded.
" All messages and errors will be ignored.
silent! helptags ALL
let mapleader = ","
